import datetime
import logging
import os
import re

import numpy as np

import bioread

from biopac.kernel.utils.helper_functions import check_biopac_properties

def subsample(biopac_dir, acq_files, props, subsample_time):
    """Performs a subsampling on a set of of biopac acq files.

    Args:
        biopac_dir (str): the biopac base directory which contains the biopac files
        acq_files (list of str): the biopac acq files
        props (list of str): the list of properties to subsample
        subsampling_time (float): the subsampling time in seconds

    Returns:
        list: a nested list whose elements are of the form (time,subsampled prop 1, subsampled prop 2 ...)
    """

    props = check_biopac_properties(biopac_dir, acq_files, props)

    # The first nested list will contain the times, while the others will contain the subsampled data for each property
    sampled_data = [[]]
    for i in range(len(props)):
        sampled_data.append([])

    # Loop over the data files
    for data_file in acq_files:

        logging.info('Processing {} file ... '.format(data_file))

        # Fetch the starting time from the filename as stated y convention of biopac files
        starting_time = re.findall(r'.*[0-9]{4}-[0-9]{2}-[0-9]{2}T([0-9]{2}_[0-9]{2}_[0-9]{2}).acq', data_file)[0]

        starting_time = datetime.datetime.strptime(starting_time, '%H_%M_%S')

        # Actually read the data
        data = bioread.read_file(os.path.join(biopac_dir, data_file))

        samples_per_second = data.named_channels[props[0]].samples_per_second

        n_points_per_block = subsample_time*samples_per_second

        if not n_points_per_block.is_integer():
            subsample_time = int(n_points_per_block)/samples_per_second
            logging.warning('The sampling time does not allow the gathering of n values where n is an integer. Will use a sampling time of {} instead.'.format(subsample_time))

        n_points_per_block = int(subsample_time*samples_per_second)
        n_points = data.named_channels[props[0]].point_count
        n_blocks = int(n_points/n_points_per_block)

        #  Build the time axis
        for i in range(n_blocks):

            time = str(starting_time + datetime.timedelta(seconds=i*subsample_time)).split(' ')[1]
            sampled_data[0].append(time)

        for idx, prop in enumerate(props):

            channel = data.named_channels[prop]

            # Performs the subsampling for the running property
            for i in range(n_blocks):

                start = i*n_points_per_block
                end = (i+1)*n_points_per_block
                average_data = np.mean(channel.data[start:end])
                sampled_data[idx+1].append(average_data)

    # Transpose the subsampled_data nested lists
    sampled_data = list(zip(*sampled_data))

    return props, sampled_data