"""This module implements a set of helper functions.
"""

import os
import pprint
import re

import bioread

from biopac.kernel.utils.errors import UnmatchingPropertySetError

def biopac_info(biopac_file):
    """Print information about the properties stored in a biopac acq file.

    Args:
        biopac_file (str): the biopac file to scan
    """

    header = bioread.read_headers(biopac_file)
    pprint.pprint(header.named_channels)

def check_biopac_properties(biopac_dir, acq_files, props):
    """Check that a list of properties are valid and can be treated alltogether.

    Args:
        biopac_dir (str): the biopac base directory which contains the biopac files
        acq_files (list of str): the biopac acq files
        props (list of str): the list of properties to check

    Returns:
        list of str: the list of valid properties
    """

    first = True
    for data_file in acq_files:

        # Reading the headers is enough for getting general informations about the properties
        data = bioread.read_headers(os.path.join(biopac_dir, data_file))

        if props is None:
            current_props = set(data.named_channels.keys())
        else:
            current_props = set(data.named_channels.keys()).intersection(props)

        if not current_props:
            raise UnmatchingPropertySetError('No valid properties selected for output')

        # properties must have the same sampling rate
        if len(set([data.named_channels[v].samples_per_second for v in current_props])) != 1:
            raise UnmatchingPropertySetError('Different samplings found for the properties selected for output')

        # properties must have the same points count
        if len(set([data.named_channels[v].point_count for v in current_props])) != 1:
            raise UnmatchingPropertySetError('Different points counts found for the properties selected for output')

        if first:
            initial_props = current_props
            first = False
        else:
            if initial_props != current_props:
                raise UnmatchingPropertySetError('The data files do not have matching property selected for output')

    props = list(initial_props)

    return props

def find_biopac_directories(root_dir):
    """Find all directories storing valid biopac acq files.

    To be valid an acq filename must have the following format .*[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}_[0-9]{2}_[0-9]{2}.acq.

    Returns:
        list of str: the biopac directories

    """

    biopac_dirs = []
    for root, dirs, _ in os.walk(root_dir):
        if dirs:
            continue
        biopac_files = sorted(glob_re(r'.*[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}_[0-9]{2}_[0-9]{2}.acq', os.listdir(root)))
        if not biopac_files:
            continue
        else:
            biopac_dirs.append(root)

    return sorted(biopac_dirs)

def glob_re(pattern, strings):
    """Filter a set of path using a given pattern.

    Returns:
        filter object: the filtered paths
    """

    return filter(re.compile(pattern).match, strings)
