class UnmatchingPropertySetError(Exception):
    """Error raised when a mismatch is found between properties or if no valid properties is found.
    """
