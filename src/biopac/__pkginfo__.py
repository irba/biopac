# **************************************************************************
#
# biopac
#
# **************************************************************************

__description__ = "Collections of softwares for working with biopac files"

__long_description__ = """biopac is a suite of prgrams for working with biopac files.
"""

__author__ = "E.C. Pellegrini"

__author_email__ = "ericpellegrini76@gmail.com"

__maintainer__ = "E.C. Pellegrini"

__maintainer_email__ = "ericpellegrini76@gmail.com"

__repo__ = "https://gitlab.com/irba/biopac"

__license__ = "GPL 3"

__version__ = "0.0.0"
