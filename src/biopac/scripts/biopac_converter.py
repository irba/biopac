#!/usr/bin/env python

import argparse
import logging
import os
import sys

import bioread

from biopac.kernel.utils.helper_functions import biopac_info, find_biopac_directories, glob_re
from biopac.kernel.utils.signal import subsample

def create_parser():
    """Returns the argument namespace after command-line parsing.
    """

    parser = argparse.ArgumentParser(description="This is biopac_converter: a program for converting from biopac acq files to PiCCO2 csv files")
    parser.add_argument("--sampling-time", "-s", dest="sampling_time", default="10", help="sampling time (in seconds)")
    parser.add_argument("--properties", "-p", dest="properties", nargs="*", default=None, help="selected properties")
    parser.add_argument("--root", "-r", dest="root_dir", help="root directories")
    parser.add_argument("--info", "-i", dest="info", help="get information about a given acq file")

    return parser

def main():

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    parser = create_parser()

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()
    

    if args.info is not None:
        biopac_info(args.info)
        sys.exit(0)

    root_dir = args.root_dir

    subsample_time = int(args.sampling_time)

    properties = args.properties

    biopac_dirs = find_biopac_directories(root_dir)

    for biopac_dir in biopac_dirs:

        logging.info('Scanning {} directory'.format(biopac_dir))

        data_files = sorted(glob_re(r'.*[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}_[0-9]{2}_[0-9]{2}.acq', os.listdir(biopac_dir)))

        properties, sampled_data = subsample(biopac_dir, data_files, properties, subsample_time)

        biopac_csv = open(os.path.join(biopac_dir, 'biopac.csv'), 'w')
        biopac_csv.write('PiCCO2;C108500156;V3.1.0.8 A\n')
        biopac_csv.write('Weight;Height;Age;Gender;category;TD cath;BSA;PBW;PBSA;t_initial;t_final\n')
        biopac_csv.write(';;;;;;;;;;\n')
        biopac_csv.write('ID;Name\n')
        biopac_csv.write(';\n')
        biopac_csv.write(';\n')
        biopac_csv.write(';\n')

        csv_props = ['Time'] + properties
        biopac_csv.write(';'.join(csv_props))
        biopac_csv.write('\n')

        # Write the subsampled data to the csv output file
        for d in sampled_data:
            biopac_csv.write(';'.join([str(v) for v in d]))
            biopac_csv.write('\n')

        biopac_csv.close()


if __name__ == '__main__':
    main()
